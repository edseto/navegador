package cat.dam.edgar.navegador;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.Build;

public class DetectConection
{
    private Context mContext;

    public DetectConection(Context mContext)
    {
        this.mContext = mContext;
    }

    public boolean isConnection()
    {
        final ConnectivityManager connectivityManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager != null) {
            if (Build.VERSION.SDK_INT >= 23) {
                //Per versions igual o superiors a Lollipop podem utilitzar el mètode getActiveNetwork() i la classe NetworkCapabilities
                final Network network = connectivityManager.getActiveNetwork();
                if (network != null) {
                    final NetworkCapabilities networkCapabilities = connectivityManager.getNetworkCapabilities(network);
                    return (networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) ||
                            networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI));
                }
            } else {
                //Per versions inferiors a Lollipop podem utilitzar mètodes obsolets
                final NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
                if (networkInfo != null) {
                    return (networkInfo.isConnected() && (networkInfo.getType() == ConnectivityManager.TYPE_MOBILE ||
                            networkInfo.getType() == ConnectivityManager.TYPE_WIFI));
                }
            }
        }
        return false;

    }
}
