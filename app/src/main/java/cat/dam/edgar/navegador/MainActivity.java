package cat.dam.edgar.navegador;

import androidx.appcompat.app.AppCompatActivity;

import android.net.http.SslError;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.net.HttpURLConnection;
import java.net.URL;

import static android.os.Build.VERSION.SDK_INT;

public class MainActivity extends AppCompatActivity
{
    private Button btn_send;
    private WebView wv_web;
    private String uri;
    private EditText et_url;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        preventNetworkError();
        setVariables();
        setListener();
    }

    private void preventNetworkError()
    {
        if (SDK_INT > 8){
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
    }

    private void setVariables()
    {
        et_url = (EditText) findViewById(R.id.et_web);
        btn_send = (Button) findViewById(R.id.btn_send);
        wv_web = (WebView) findViewById(R.id.wv_web);

    }

    private void setListener()
    {
        btn_send.setOnClickListener(new View.OnClickListener() {
            final String noConnect = getResources().getString(R.string.no_connect);
            @Override
            public void onClick(View v) {
                if(haveConnection()) showWeb();
                else createToast(noConnect);
            }
        });
    }

    private boolean haveConnection()
    {
        DetectConection connection = new DetectConection(getApplicationContext());

        return connection.isConnection();
    }

    private void createToast(String description)
    {
        Toast.makeText(getApplicationContext(), description, Toast.LENGTH_LONG).show();
    }

    private void showWeb()
    {
        final String noExists = getResources().getString(R.string.no_web);
        uri = et_url.getText().toString();
        if(webExists(uri)) openWeb();
        else createToast(noExists);
    }

    private void openWeb()
    {
        setSettings();
        wv_web.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView webView, String url) {
                webView.loadUrl(url);
                return true;
            }
            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                handler.proceed();
            }
            @Override
            public void onPageFinished(WebView view, final String url) {
                final String webOk = getResources().getString(R.string.web_ok);
                createToast(webOk);
            }
        });
        wv_web.loadUrl(uri);
        createToast(getResources().getString(R.string.web_ok));
    }

    private void setSettings()
    {
        WebSettings webSettings = wv_web.getSettings();

        webSettings.setJavaScriptEnabled(true);
        webSettings.setBuiltInZoomControls(true);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setUseWideViewPort(true);
        webSettings.setDomStorageEnabled(true);
    }

    public boolean webExists(String uri) {
        try {
            HttpURLConnection.setFollowRedirects(false);
            HttpURLConnection con = (HttpURLConnection) new URL(uri).openConnection();
            con.setRequestMethod("HEAD");
            return (con.getResponseCode() == HttpURLConnection.HTTP_OK);
        } catch (Exception e) {
            wv_web.loadData("<HTML><BODY><H3></H3></BODY></HTML>","text/html","utf-8");
            return false;
        }
    }
}